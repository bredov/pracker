package szpool

import (
	"sort"
	"sync"
)

type SizedPool struct {
	sizes []int
	pools []sync.Pool
}

func New(sizes ...int) SizedPool {
	sort.Ints(sizes)

	return SizedPool{
		sizes: sizes,
		pools: make([]sync.Pool, len(sizes)),
	}
}

func (p *SizedPool) Retain(size int) Buffer {
	for i := range p.sizes {
		if size <= p.sizes[i] {
			iumem := p.pools[i].Get()

			if iumem == nil {
				iumem = make([]byte, p.sizes[i])
			}

			umem := iumem.([]byte)

			return Buffer{
				umem: umem,
				Mem:  umem[:size],
			}
		}
	}
	panic("szpool: not found pool that handles given size")
}

func (p *SizedPool) Return(b Buffer) {
	for i := range p.sizes {
		if len(b.umem) == p.sizes[i] {
			p.pools[i].Put(b.umem)
		}
	}
}

func (p *SizedPool) With(size int, f func(Buffer)) {
	b := p.Retain(size)
	defer p.Return(b)

	f(b)
}

type Buffer struct {
	// Underlying memory buffer which shall be returned to pool
	umem []byte

	// Limited slice to match requested size
	Mem []byte
}

package httptracker

import (
	"gitlab.com/bredov/pracker/pracker"

	"github.com/anacrolix/torrent/tracker"
)

func Announce(req *pracker.AnnRequest, resp *pracker.AnnResponse) error {
	anareq := &tracker.AnnounceRequest{
		InfoHash:   [20]byte(req.InfoHash),
		PeerId:     [20]byte(req.PeerID),
		Downloaded: req.Downloaded,
		Left:       uint64(req.Left),
		Uploaded:   req.Uploaded,
		Event:      tracker.AnnounceEvent(req.Event),
		IPAddress:  int32(req.IPAddr),
		Key:        int32(req.Key),
		NumWant:    req.NumWant,
		Port:       req.Port,
	}

	u := string(req.URLData[1:])
	anares, err := tracker.Announce(u, anareq)
	if err != nil {
		return err
	}

	resp.Interval = anares.Interval
	resp.Leechers = anares.Leechers
	resp.Seeders = anares.Seeders
	resp.Peers = make([]pracker.Peer, len(anares.Peers))

	for i := range anares.Peers {
		ipbs := anares.Peers[i].IP.To4()

		var ip uint32

		ip |= uint32(ipbs[0]) << 24
		ip |= uint32(ipbs[1]) << 16
		ip |= uint32(ipbs[2]) << 8
		ip |= uint32(ipbs[3])

		resp.Peers[i] = pracker.Peer{
			IP:   ip,
			Port: uint16(anares.Peers[i].Port),
		}
	}

	resp.Action = pracker.ActionAnnounce
	resp.TxID = req.TxID

	return nil
}

func Scrape(req *pracker.ScrapeRequest, resp *pracker.ScrapeResponse) error {
	return nil
}

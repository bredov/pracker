package pracker

import (
	"encoding/binary"
	"errors"
	"fmt"
)

// ErrShortPacket indicates incomplete packet.
var ErrShortPacket = errors.New("packet is too short")

var endian = binary.BigEndian

// Event is an event which caused announce request.
type Event int32

// Event constants.
const (
	EventNone Event = iota
	EventCompleted
	EventStarted
	EventStopped
)

var eventStringerConstants = [...]string{
	EventNone:      "none",
	EventCompleted: "completed",
	EventStarted:   "started",
	EventStopped:   "stopped",
}

func (e Event) String() string {
	return eventStringerConstants[e]
}

// Action is a type of action to be performed.
type Action int32

// Action constants.
const (
	ActionConnect Action = iota
	ActionAnnounce
	ActionScrape
	ActionError
)

var actionStringerConstants = [...]string{
	ActionConnect:  "connect",
	ActionAnnounce: "announce",
	ActionScrape:   "scrape",
	ActionError:    "error",
}

func (a Action) String() string {
	return actionStringerConstants[a]
}

// OptionType is a type of extension option.
type OptionType uint8

// Option types constants.
const (
	OptionEndOfOptions OptionType = iota
	OptionNOP
	OptionURLData
)

var optionTypeStringerConstants = [...]string{
	OptionEndOfOptions: "end of options",
	OptionNOP:          "nop",
	OptionURLData:      "url data",
}

func (o OptionType) String() string {
	return optionTypeStringerConstants[o]
}

// PeerID represents a 20 byte peer id.
type PeerID [20]byte

// InfoHash represents a 20 byte info hash.
type InfoHash [20]byte

// Peer represents peer with IP and Port.
type Peer struct {
	IP   uint32
	Port uint16
}

func (p Peer) String() string {
	return fmt.Sprintf("%s:%d", ipString(p.IP), p.Port)
}

func ipString(ip uint32) string {
	ip0 := ip >> 24
	ip1 := (ip >> 16) & 0xff
	ip2 := (ip >> 8) & 0xff
	ip3 := ip & 0xff

	return fmt.Sprintf("%d.%d.%d.%d", ip0, ip1, ip2, ip3)
}

// ScrapeInfo represents scrape information.
type ScrapeInfo struct {
	Seeders   int32
	Completed int32
	Leechers  int32
}

// URLData represents URL data for use in BEP-41
type URLData []byte

func (d URLData) String() string {
	return string(d)
}

// Response is a common interface for all kinds of response.
type Response interface {
	Len() int
	Marshal([]byte)
	Pretty() string
}

// ReqHeader is a request header which contains connection id,
// action and transaction id.
type ReqHeader struct {
	ConnID int64
	Action Action
	TxID   int32
}

func (r *ReqHeader) Pretty() string {
	return fmt.Sprintf("id: %#0x, action: %s, txID: %#0x", r.ConnID, r.Action, r.TxID)
}

// ParseReqHeader parses b and obtains ReqHeader from it.
// Returns ErrShortPacket in case of insufficient data.
func ParseReqHeader(r *ReqHeader, b []byte) error {
	if len(b) < 16 {
		return ErrShortPacket
	}
	r.ConnID = int64(endian.Uint64(b))
	r.Action = Action(endian.Uint32(b[8:]))
	r.TxID = int32(endian.Uint32(b[12:]))
	return nil
}

// ConnRequest (Big-Endian)
type ConnRequest struct {
	ReqHeader
}

func (r *ConnRequest) Pretty() string {
	return fmt.Sprintf("ConnRequest{%s}", r.ReqHeader.Pretty())
}

// ParseConnRequest parses b and obtains ConnRequest from it.
// Returns ErrShortPacket in case of insufficient data.
func ParseConnRequest(r *ConnRequest, b []byte) error {
	if len(b) < 16 {
		return ErrShortPacket
	}
	r.ConnID = int64(endian.Uint64(b))
	r.Action = Action(endian.Uint32(b[8:]))
	r.TxID = int32(endian.Uint32(b[12:]))
	return nil
}

// ConnResponse (Big-Endian)
type ConnResponse struct {
	Action Action
	TxID   int32
	ConnID int64
}

func (r *ConnResponse) Pretty() string {
	return fmt.Sprintf("ConnResponse{action: %s, txID: %#0x, id: %#0x}",
		r.Action, uint32(r.TxID), r.ConnID)
}

// Len returns size of packet in bytes.
func (c *ConnResponse) Len() int {
	return 16
}

// Marshal encodes ConnResponse into an array of bytes.
func (c *ConnResponse) Marshal(b []byte) {
	endian.PutUint32(b[0:], uint32(c.Action))
	endian.PutUint32(b[4:], uint32(c.TxID))
	endian.PutUint64(b[8:], uint64(c.ConnID))
}

type AnnRequest struct {
	ReqHeader
	InfoHash   InfoHash
	PeerID     PeerID
	Downloaded int64
	Left       int64
	Uploaded   int64
	Event      Event
	IPAddr     uint32
	Key        uint32
	NumWant    int32
	Port       uint16
	URLData    URLData
}

func (r *AnnRequest) Pretty() string {
	return fmt.Sprintf("AnnRequest{%s, event: %s, ip: %s:%d, urldata: %s}",
		r.ReqHeader.Pretty(), r.Event, ipString(r.IPAddr), r.Port, r.URLData)
}

func ParseAnnRequest(r *AnnRequest, b []byte) error {
	if len(b) < 98 {
		return ErrShortPacket
	}

	// Parse request header.
	r.ConnID = int64(endian.Uint64(b[0:]))
	r.Action = Action(endian.Uint32(b[8:]))
	r.TxID = int32(endian.Uint32(b[12:]))

	copy(r.InfoHash[:], b[16:])
	copy(r.PeerID[:], b[36:])
	r.Downloaded = int64(endian.Uint64(b[56:]))
	r.Left = int64(endian.Uint64(b[64:]))
	r.Uploaded = int64(endian.Uint64(b[72:]))
	r.Event = Event(endian.Uint32(b[80:]))
	r.IPAddr = endian.Uint32(b[84:])
	r.Key = endian.Uint32(b[88:])
	r.NumWant = int32(endian.Uint32(b[92:]))
	r.Port = endian.Uint16(b[96:])

	if len(b) > 98 {
		b = b[98:]
		for i := 0; i < len(b); i++ {
			switch OptionType(b[i]) {
			default:
				i += skipOption(b[i:])
			case OptionEndOfOptions:
				break
			case OptionNOP:
				continue
			case OptionURLData:
				i += parseURLData(r, b[i:])
			}
		}
	}

	return nil
}

// skipOption returns the total length in bytes of an option starting
// at b without option type byte itself. Thus, option type
// byte must be consumed by caller.
// | OPT | LEN | DATA ... |
// |  1  |  1  |     N    |
// return value = N + 1
func skipOption(b []byte) int {
	size := int(b[1])

	// option type byte would be skipped in enclosing for-loop,
	// so just skip size + length byte
	return size + 1
}

// parseURLData parses option starting at b into r and returns total length
// of option in bytes without option type byte itself. Thus, option type
// byte must be consumed by caller.
// | OPT | LEN | DATA ... |
// |  1  |  1  |     N    |
// return value = N + 1
func parseURLData(r *AnnRequest, b []byte) int {
	size := int(b[1])
	r.URLData = append(r.URLData, b[2:size+2]...)

	// option type byte would be skipped in enclosing for-loop,
	// so just skip size + length byte
	return size + 1
}

type AnnResponse struct {
	Action   Action
	TxID     int32
	Interval int32
	Leechers int32
	Seeders  int32
	Peers    []Peer
}

func (r *AnnResponse) Pretty() string {
	peers := make([]string, len(r.Peers))
	for i := range r.Peers {
		peers[i] = r.Peers[i].String()
	}
	return fmt.Sprintf("AnnResponse{action: %s, txID: %#0x, peers: %s}",
		r.Action, uint32(r.TxID), peers)
}

// Len returns size of packet in bytes
func (r *AnnResponse) Len() int {
	return 20 + 6*len(r.Peers)
}

func (r *AnnResponse) Marshal(b []byte) {
	endian.PutUint32(b[0:], uint32(r.Action))
	endian.PutUint32(b[4:], uint32(r.TxID))
	endian.PutUint32(b[8:], uint32(r.Interval))
	endian.PutUint32(b[12:], uint32(r.Leechers))
	endian.PutUint32(b[16:], uint32(r.Seeders))

	for i := range r.Peers {
		endian.PutUint32(b[20+6*i:], r.Peers[i].IP)
		endian.PutUint16(b[24+6*i:], r.Peers[i].Port)
	}
}

type ScrapeRequest struct {
	ReqHeader
	InfoHashes []InfoHash
}

func (r *ScrapeRequest) Pretty() string {
	return fmt.Sprintf("ScrapeRequest{%s}", r.ReqHeader.Pretty())
}

func ParseScrapeRequest(r *ScrapeRequest, b []byte) error {
	if len(b) < 16 {
		return ErrShortPacket
	}

	// Parse request header.
	r.ConnID = int64(endian.Uint64(b[0:]))
	r.Action = Action(endian.Uint32(b[8:]))
	r.TxID = int32(endian.Uint32(b[12:]))

	b = b[16:]

	r.InfoHashes = make([]InfoHash, len(b)/20)

	for i := range r.InfoHashes {
		copy(r.InfoHashes[i][:], b[20*i:])
	}

	return nil
}

type ScrapeResponse struct {
	Action      Action
	TxID        int32
	ScrapeInfos []ScrapeInfo
}

func (r *ScrapeResponse) Pretty() string {
	return fmt.Sprintf("ScrapeResponse{action: %s, txID: %#0x", r.Action, uint32(r.TxID))
}

// Len returns size of packet in bytes
func (r *ScrapeResponse) Len() int {
	return 8 + 12*len(r.ScrapeInfos)
}

func (r *ScrapeResponse) Marshal(b []byte) {
	endian.PutUint32(b[0:], uint32(r.Action))
	endian.PutUint32(b[4:], uint32(r.TxID))

	for i := range r.ScrapeInfos {
		endian.PutUint32(b[8+12*i:], uint32(r.ScrapeInfos[i].Seeders))
		endian.PutUint32(b[12+12*i:], uint32(r.ScrapeInfos[i].Completed))
		endian.PutUint32(b[16+12*i:], uint32(r.ScrapeInfos[i].Leechers))
	}
}

type ErrorResponse struct {
	Action  Action
	TxID    int32
	Message []byte
}

func (r *ErrorResponse) Pretty() string {
	return fmt.Sprintf("ErrorResponse{action: %s, txID: %#0x, message: %s",
		r.Action, uint32(r.TxID), r.Message)
}

// Len returns size of packet in bytes
func (r *ErrorResponse) Len() int {
	return 8 + len(r.Message)
}

func (r *ErrorResponse) Marshal(b []byte) {
	endian.PutUint32(b[0:], uint32(r.Action))
	endian.PutUint32(b[4:], uint32(r.TxID))
	copy(b[8:], r.Message)
}

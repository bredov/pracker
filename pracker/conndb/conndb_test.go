package conndb

import "testing"

func TestNewConnMap(t *testing.T) {
	cm := newConnMap()

	if cm == nil {
		t.Errorf("newConnMap doesn't create anything (value is nil)")
	}

	if len(cm.at) != 1 {
		t.Errorf("len(cm.at) expected 1 got %v", len(cm.at))
	}

	if c, ok := cm.at[ConnectingID]; !ok {
		t.Errorf("new conn map doesn't contain special connecting id value")
	} else if c == nil {
		t.Errorf("ConnectingID is nil value")
	} else if c.ID != ConnectingID {
		t.Errorf("expected special connecting id to be %v, got %v", ConnectingID, c.ID)
	}
}

func TestRegisterNew(t *testing.T) {
	cm := newConnMap()

	cm.RegisterNew()

	if len(cm.at) != 2 {
		t.Errorf("len(cm.at) expected 1 got %v", len(cm.at))
	}
}

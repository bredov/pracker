package conndb

import (
	"math/rand"
	"sync"
	"time"
)

const ConnectingID = 0x41727101980

var connMap = *newConnMap()

type connMapT struct {
	sync.RWMutex
	at map[int64]*Conn
}

func newConnMap() *connMapT {
	return &connMapT{
		at: map[int64]*Conn{
			ConnectingID: &Conn{ID: ConnectingID},
		},
	}
}

// RegisterNew creates new connection with unique ID and a 1-minute timeout.
// Function registers connection in global connection map and returns it.
// It is safe to use this function from multiple goroutines at the same time.
//CONSIDER: test for performance in case of lot of goroutines invokes it.
func (cm *connMapT) RegisterNew() *Conn {
	id := rand.Int63()

	cm.Lock()
	for _, has := cm.at[id]; has; _, has = cm.at[id] {
		id = rand.Int63()
	}

	c := &Conn{
		ID:        id,
		keepalive: make(chan struct{}, 2),
	}

	cm.at[id] = c
	cm.Unlock()

	go c.dieOnTimeout(cm)

	return c
}

// RegisterNew creates new connection with unique ID and a 1-minute timeout.
// Function registers connection in connection map and returns it.
// It is safe to use this function from multiple goroutines at the same time.
func RegisterNew() *Conn {
	return connMap.RegisterNew()
}

// Get returns a connection from connection map.
// It returns nil if no connection with given ID was found.
func (cm *connMapT) Get(id int64) *Conn {
	cm.RLock()
	defer cm.RUnlock()

	return cm.at[id]
}

// Get returns a connection from global connection map.
// It returns nil if no connection with given ID was found.
func Get(id int64) *Conn {
	return connMap.Get(id)
}

// Delete remove a connection with given connection ID
// from connection map.
func (cm *connMapT) Delete(id int64) {
	cm.Lock()
	delete(cm.at, id)
	cm.Unlock()
}

// Delete remove a connection with given connection ID
// from global connection map.
func Delete(id int64) {
	connMap.Delete(id)
}

// Conn is a connection as specified in BEP-0015
type Conn struct {
	ID        int64
	keepalive chan struct{}
}

func (c *Conn) dieOnTimeout(cm *connMapT) {
	for {
		select {
		case <-time.After(time.Minute):
			cm.Delete(c.ID)
			return
		case <-c.keepalive:
		}
	}
}

// Alive prolongates the live of connection for 1 minute more.
func (c *Conn) Alive() {
	c.keepalive <- struct{}{}
}

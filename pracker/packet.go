package pracker

import (
	"net"
)

// Packet is a UDP packet.
type Packet struct {
	// Addr is a sender UDP address.
	Addr   *net.UDPAddr
	Buffer []byte
	Len    int
}

// Payload returns a slice
func (p *Packet) Payload() []byte {
	return p.Buffer[:p.Len]
}

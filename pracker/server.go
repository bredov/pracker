package pracker

import (
	"net"

	"gitlab.com/bredov/pracker/szpool"
)

type configF func(*configT) error
type configT struct{}

// Server handles UDP packet transmission.
type Server struct {
	Addr *net.UDPAddr
	conn *net.UDPConn
	conf configT
	pool szpool.SizedPool
}

// NewServerStart starts a listening on given address
// and returns a Server instance to manage packet transmission.
// Server may be configured using config functions.
// Currently, there is no config functions available.
func NewServerStart(rawaddr string, fs ...configF) (*Server, error) {
	addr, err := net.ResolveUDPAddr("udp", rawaddr)
	if err != nil {
		return nil, err
	}

	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		return nil, err
	}

	// Currently unused.
	conf := configT{}

	for _, f := range fs {
		if err := f(&conf); err != nil {
			return nil, err
		}
	}

	s := &Server{
		Addr: addr,
		conn: conn,
		conf: conf,
		pool: szpool.New(32, 128, 512, 2048),
	}

	return s, nil
}

// ReadPacket reads a UDP packet from server's conn
// into buffer and return a &Packet{} instance holding
// read information and provided buffer in field Buffer.
func (s *Server) ReadPacket(buffer []byte) (*Packet, error) {
	n, sender, err := s.conn.ReadFromUDP(buffer)
	if err != nil {
		return nil, err
	}

	p := &Packet{
		Addr:   sender,
		Buffer: buffer,
		Len:    n,
	}

	return p, nil
}

// Reply sends a Response to a Packet sender.
// It returns number of bytes sent and any errors occured
// while sending.
func (s *Server) Reply(p *Packet, r Response) (n int, err error) {
	b := s.pool.Retain(r.Len())
	defer s.pool.Return(b)

	r.Marshal(b.Mem)

	return s.conn.WriteToUDP(b.Mem, p.Addr)
}

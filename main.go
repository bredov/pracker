package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/honeybadger-io/honeybadger-go"

	"gitlab.com/bredov/pracker/httptracker"
	"gitlab.com/bredov/pracker/pracker"
	"gitlab.com/bredov/pracker/pracker/conndb"
	"gitlab.com/bredov/pracker/szpool"
)

const defaultMaxErrors = 32
const maxPacketSize = 4096 // should be enough for extensions

var packetPool = szpool.New(maxPacketSize)

var cmdline struct {
	addr    string
	retries int
	verbose bool
}

var slog *log.Logger

type replyF func(p *pracker.Packet, r pracker.Response) (int, error)

func init() {
	flag.StringVar(&cmdline.addr, "addr", "0.0.0.0:0", "address and port to listen on.")
	flag.IntVar(&cmdline.retries, "retries", defaultMaxErrors, "number of retries on consequent read failures.")
	flag.BoolVar(&cmdline.verbose, "verbose", false, "enable verbose mode")

	slog = log.New(os.Stderr, "[pracker] ", log.Ldate|log.Ltime|log.Lmicroseconds)
}

func main() {
	defer honeybadger.Monitor()

	flag.Parse()

	s, err := pracker.NewServerStart(cmdline.addr)
	if err != nil {
		slog.Fatal("Failed to start UDP server: ", err)
	}
	slog.Printf("Listening on %s", s.Addr)

	// no more than cmdline.retries subsequent errors
	for errors := 0; errors < cmdline.retries; {
		buffer := make([]byte, maxPacketSize)

		p, err := s.ReadPacket(buffer)
		if err != nil {
			slog.Print("Cannot read UDP packet: ", err)
			errors++
			continue
		}
		errors = 0

		go reply(s.Reply, p)
	}
}

func reply(rep replyF, p *pracker.Packet) {
	defer func() {
		if err := recover(); err != nil {
			honeybadger.Notify(err, honeybadger.Context{
				"IP": p.Addr.String(),
			})

			slog.Printf("Failed to handle request[%s]: %s", p.Addr, err)

			if err := tryReplyInternalError(rep, p); err != nil {
				slog.Printf("Failed to inform client[%s] about error: %s", p.Addr, err)
			}
		}
	}()

	var req pracker.ReqHeader

	pracker.ParseReqHeader(&req, p.Payload())

	switch req.Action {
	default:
		replyErrorf(rep, p, "unsupported action %d", req.Action)
	case pracker.ActionConnect:
		replyConnect(rep, p)
	case pracker.ActionAnnounce:
		replyAnnounce(rep, p)
	case pracker.ActionScrape:
		replyScrape(rep, p)
	}
}

func replyConnect(rep replyF, p *pracker.Packet) {
	var req pracker.ConnRequest

	pracker.ParseConnRequest(&req, p.Payload())

	if req.ConnID != conndb.ConnectingID {
		replyErrorf(rep, p, "unsupported protocol %#x", uint64(req.ConnID))
		return
	}

	c := conndb.RegisterNew()

	vprintf("New connection[TX:%#0x ID:%#0x]", uint32(req.TxID), uint64(c.ID))

	resp := pracker.ConnResponse{
		Action: req.Action,
		ConnID: c.ID,
		TxID:   req.TxID,
	}

	rep(p, &resp)
}

func replyAnnounce(rep replyF, p *pracker.Packet) {
	var req pracker.AnnRequest

	pracker.ParseAnnRequest(&req, p.Payload())

	if req.ConnID == conndb.ConnectingID {
		vprintf("ConnectingID can be seen only in CONNECT phase [Tx: %#0x]", req.TxID)
		replyErrorf(rep, p, "Client have sent connecting id during announce phase")
		return
	}

	if c := conndb.Get(req.ConnID); c != nil {
		c.Alive()
	} else {
		vprintf("Announce failed: [%#0x] already timed out", c.ID)
		replyErrorf(rep, p, "Client must reconnect, id %#0x timed out", c.ID)
		return
	}

	if req.URLData == nil {
		vprintf("[%#0x %s] didn't provide tracker address", uint64(req.ConnID), p.Addr)
		replyErrorf(rep, p, `Client must provide tracker address through URL data (BEP-41)`)
		return
	}

	vprintf("Announce[TX:%#0x] request: %+v", uint32(req.TxID), req.Pretty())

	var resp pracker.AnnResponse

	if err := httptracker.Announce(&req, &resp); err != nil {
		vprintf("Announce error: %s", err)
		replyErrorf(rep, p, "Announce error occured: %s", err)
		return
	}

	vprintf("Announce[TX:%#0x] response: %+v", uint32(resp.TxID), resp.Pretty())

	rep(p, &resp)
}

func replyScrape(rep replyF, p *pracker.Packet) {
	var req pracker.ScrapeRequest

	pracker.ParseScrapeRequest(&req, p.Payload())

	if req.ConnID == conndb.ConnectingID {
		vprintf("ConnectingID can be seen only in CONNECT phase [Tx: %#0x]", req.TxID)
		replyErrorf(rep, p, "Client have sent connecting id during announce phase")
		return
	}

	if c := conndb.Get(req.ConnID); c != nil {
		c.Alive()
	} else {
		vprintf("Scrape failed: [%#0x] already timed out", c.ID)
		replyErrorf(rep, p, "Client must reconnect, id %#0x timed out", c.ID)
		return
	}

	var resp pracker.ScrapeResponse

	if err := httptracker.Scrape(&req, &resp); err != nil {
		vprintf("Scrape error: %s", err)
		replyErrorf(rep, p, "Scrape error occured: %s", err)
		return
	}

	vprintf("Scrape[TX:%0x] response: %+v", uint32(resp.TxID), resp.Pretty())

	rep(p, &resp)
}

func replyErrorf(rep replyF, p *pracker.Packet, format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)

	var req pracker.ReqHeader
	pracker.ParseReqHeader(&req, p.Payload())

	resp := pracker.ErrorResponse{
		Action:  pracker.ActionError,
		TxID:    req.TxID,
		Message: []byte(message),
	}

	rep(p, &resp)
}

func tryReplyInternalError(rep replyF, p *pracker.Packet) (err error) {
	defer func() {
		if rerr := recover(); rerr != nil {
			err = fmt.Errorf("%s", rerr)
		}
	}()

	replyErrorf(rep, p, "Internal Server Error")

	return nil
}

func vprintf(format string, args ...interface{}) {
	if cmdline.verbose {
		slog.Printf(format, args...)
	}
}

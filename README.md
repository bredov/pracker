# Pracker - UDP proxy tracker.

This project is a proxy server which helps to talk to a HTTP-tracker
through a UDP-tracker (BEP-0015) protocol. This enables clients which
are banned from using HTTP protocol to communicate to HTTP-trackers.

For any given HTTP-tracker `http://<http-tracker-address>` you can proxy request
by sending them to `udp://<pracker-address>/http://<http-tracker-address>`
